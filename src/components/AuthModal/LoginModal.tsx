import FormComponent from '../Form'
import CustomInput from '../CustomInput'
import CustomButton from '../CustomButton'
import * as Yup from 'yup'
import { useAppDispatch } from '~root/config/redux/reduxHooks'
import { login } from '~root/config/redux/slices/auth'
import { useNavigate } from 'react-router-dom'
import { LOCAL_STORAGE_TOKEN_LOGIN } from '~root/constants'

const LoginSchema = Yup.object().shape({
    email: Yup.string().email('This email is invalid').required('This field is required'),
    password: Yup.string()
        .min(6, 'Password must be greater than 6 characters')
        .required('This field cannot be empty'),
})

interface ILoginValues {
    email: string
    password: string
}

const DEFAULT_LOGIN_VALUE: ILoginValues = {
    email: '',
    password: '',
}

export default function LoginModal() {
    const dispatch = useAppDispatch()
    const navigate = useNavigate()

    const handleNavigateToSignUp = () => {
        navigate('/signup', { replace: false })
    }

    return (
        <div className='bg-white rounded-md w-full flex flex-col'>
            <FormComponent
                initialValues={DEFAULT_LOGIN_VALUE}
                validationSchema={LoginSchema}
                onSubmit={async (values) => {
                    const res = await dispatch(login(values))
                    if (res.meta.requestStatus === 'fulfilled') {
                        navigate('/', { replace: true })
                    }
                    localStorage.setItem(
                        LOCAL_STORAGE_TOKEN_LOGIN,
                        JSON.stringify({ ...res.payload, id: res.payload.userId }),
                    )
                }}
            >
                <CustomInput name='email' label='Email' type='email' style='TextField' />
                <CustomInput name='password' label='Password' type='password' style='TextField' />
                <CustomButton
                    className='w-full rounded-md text-xs py-4 font-semibold mt-5 bg-primary/90 hover:cursor-pointer hover:bg-primary transition-colors'
                    title='LOG IN'
                    submit
                />
            </FormComponent>
            <span className='my-4 text-base px-6 '>
                {' By logging in you agree to 3D Platform"s'}
                <strong className='text-primary font-normal'> Terms of Service</strong> and
                <strong className='text-primary font-normal'> Privacy Policy</strong>
            </span>
            <div className='flex items-center justify-center my-2'>
                <p className='text-base mr-2'>New to 3D Platform?</p>
                <span
                    className=' font-medium text-primary hover:underline cursor-pointer'
                    onClick={handleNavigateToSignUp}
                >
                    Create an account
                </span>
            </div>
        </div>
    )
}
