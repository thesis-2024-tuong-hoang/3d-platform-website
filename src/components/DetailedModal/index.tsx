import Overlay from '../Overlay'

export default function DetailedModal() {
    return (
        <Overlay>
            <div className='bg-white w-5/6 h-5/6 rounded-md'></div>
        </Overlay>
    )
}
