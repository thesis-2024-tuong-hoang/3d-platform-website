export const LOCAL_STORAGE_TOKEN_LOGIN = 'serverToken'
export const BASE_API_URL = 'http://localhost:3002'
export const BASE_3D_PROCESSOR_API_URL = 'http://localhost:3005'
export const BASE_EXAMPLE_API_URL = ''
export const DEFAULT_USER_AVATAR =
    'https://as2.ftcdn.net/v2/jpg/03/31/69/91/1000_F_331699188_lRpvqxO5QRtwOM05gR50ImaaJgBx68vi.jpg'
