import { useNavigate } from 'react-router-dom'

export default function LogoSection() {
    const navigate = useNavigate()

    const navigateToHomePage = () => {
        navigate('/', { replace: false })
        // setOpenTooltip(false)
    }

    return (
        <div
            className='flex items-center text-2xl hover:cursor-pointer '
            onClick={navigateToHomePage}
        >
            <h3 className='text-orange-400 mr-2'>3D</h3>
            <h3 className=''>Platform</h3>
        </div>
    )
}
