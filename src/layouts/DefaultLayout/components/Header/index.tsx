import React, { useState, useCallback } from 'react'
import SearchBar from './SearchBar'
import axios from 'axios'
import { BASE_API_URL } from '~root/constants'
import ModelCard from '~root/components/ModelCard'
import { IModelCard } from '~root/screen/DetailedModelSrceen'
import { IoCloseOutline } from 'react-icons/io5'
import LogoSection from './LogoSection'
import MenuSection from './MenuSection'

export default function Header() {
    const [searchedModels, setSearchedModels] = useState<any[]>([])
    const closeRef = React.useRef<HTMLInputElement & { clear: () => void }>(null)

    const getAllModelsBySearchvalue = useCallback(async (searchValue: string) => {
        const response = await axios({
            method: 'GET',
            baseURL: `${BASE_API_URL}/process-data/3d-models/all`,
        })
        const searchedModels = response.data.models.filter((model: any) =>
            model.name.toLowerCase().includes(searchValue),
        )

        setSearchedModels(searchedModels)
    }, [])

    const handleClearSearch = () => {
        setSearchedModels([])
        if (closeRef.current) {
            closeRef.current.clear()
        }
    }

    return (
        <>
            <div className='w-full bg-white p-4 shadow-header border-b-[1px] border-solid border-black/10 relative'>
                <div className='flex items-center justify-between'>
                    <LogoSection />
                    <SearchBar ref={closeRef} searchFunction={getAllModelsBySearchvalue} />
                    <MenuSection />
                </div>
                {searchedModels.length !== 0 && (
                    <div className='w-screen h-fit h-max-[600px] bg-white absolute top-[101%] left-0 z-50 shadow-lg p-8'>
                        <div className='grid grid-cols-4 gap-8 '>
                            {searchedModels.map((model: IModelCard) => (
                                <ModelCard key={model.id} model={model} />
                            ))}
                        </div>
                        <div
                            className='w-16 h-16 hover:text-primary cursor-pointer flex items-center justify-center bg-white absolute bottom-0 left-1/2 -translate-x-1/2 translate-y-1/2 rounded-full shadow-2xl '
                            onClick={handleClearSearch}
                        >
                            <IoCloseOutline className='text-4xl hover:text-primary' />
                        </div>
                    </div>
                )}
            </div>
        </>
    )
}
