import { IoEyeOutline } from 'react-icons/io5'
import { IoIosStarOutline } from 'react-icons/io'
import { MdOutlineFileDownload } from 'react-icons/md'
import { AiOutlineQuestionCircle } from 'react-icons/ai'
import { AiOutlineClockCircle } from 'react-icons/ai'
import { Canvas } from '@react-three/fiber'
import { OrbitControls, Environment, Bounds } from '@react-three/drei'
import { useLocation } from 'react-router-dom'
import Model from '~root/components/Model'
import { useCallback, useLayoutEffect, useState } from 'react'
import axios from 'axios'
import { BASE_API_URL, DEFAULT_USER_AVATAR } from '~root/constants'
import ModelHorizontalCard from '~root/components/ModelHorizontalCard'
import { supabase } from '~root/config/supabase'
import DownloadLink from '~root/components/DownloadLink'

import classNames from 'classnames'

export interface IModelCard {
    created_at: string
    description: string
    downloaded: number
    file_name: string
    id: string
    liked: number
    name: number
    stars: number
    user: any
    user_id: number
    viewed: number
}

interface IDetailedModalScreenProps {
    modelUrl?: string
    name?: string
    type?: string
    description?: string
}

export const formatDate = (dateInput: string) => {
    const date = new Date(dateInput)
    const options: any = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
    }
    return date.toLocaleTimeString('en-us', options)
}

export default function DetailedModalScreen(props: IDetailedModalScreenProps) {
    const { pathname } = useLocation()

    const [model, setModel] = useState<IModelCard>()
    const [suggestModelList, setSuggestModelList] = useState<any>()
    const [star, setStar] = useState<boolean>(false)
    const [starNumber, setStarNumber] = useState<number>(0)

    const [author, setAuthor] = useState<any>()

    const modelName = pathname?.split('/')[2]

    const getModel = useCallback(async () => {
        const res = await axios({
            method: 'GET',
            baseURL: BASE_API_URL + `/process-data/3d-models/${modelName}`,
        })
        // Add view
        const model = res.data.model

        await supabase
            .from('models')
            .update({ viewed: model.viewed + 1 })
            .eq('id', model.id)

        setModel(res.data.model)
        setAuthor(res.data.user)
        setStarNumber(res.data.model.downloaded)
    }, [modelName])

    const starModel = async () => {
        const res = await axios({
            method: 'GET',
            baseURL: BASE_API_URL + `/process-data/3d-models/${modelName}`,
        })
        const model = res.data.model
        if (star) {
            await supabase
                .from('models')
                .update({ stars: starNumber - 1 })
                .eq('id', model.id)
            setStar(false)
            setStarNumber((currentNumber) => currentNumber - 1)
        } else {
            await supabase
                .from('models')
                .update({ stars: starNumber + 1 })
                .eq('id', model.id)
            setStar(true)
            setStarNumber((currentNumber) => currentNumber + 1)
        }
    }

    const getAllModels = useCallback(async () => {
        const response = await axios({
            method: 'GET',
            baseURL: `${BASE_API_URL}/process-data/3d-models/all`,
        })

        const suggestedList = response.data.models.filter(
            (model: any) => model.file_name !== modelName,
        )

        setSuggestModelList(suggestedList)
    }, [modelName])

    useLayoutEffect(() => {
        getModel()
        getAllModels()
    }, [getAllModels, getModel])

    return (
        <div className='w-full flex-grow bg-white rounded-sm p-8 pb-0 overflow-scroll'>
            <div className='grid grid-cols-6 h-full'>
                <div className='col-span-4  h-[448px]'>
                    <Canvas className='w-10 h-10'>
                        <Environment preset='warehouse' background blur={0.9} />
                        <group>
                            <Bounds fit margin={2}>
                                <Model
                                    url={`https://xpvurtyfnuyuvkehgqoo.supabase.co/storage/v1/object/public/3D-models/${modelName}/scene.gltf`}
                                />
                            </Bounds>
                        </group>
                        <OrbitControls enablePan={false} makeDefault />
                    </Canvas>
                    <div className='flex flex-col py-5'>
                        <div className=' mb-5'>
                            <h4 className='text-2xl'>{model?.name}</h4>
                            <p className='text-sm'>3D Model</p>
                        </div>

                        <div className='flex justify-between items-center pb-3'>
                            <div className='flex items-center'>
                                <img
                                    className='w-12 h-12 rounded-full mr-3'
                                    src={author?.avatar ? author?.avatar : DEFAULT_USER_AVATAR}
                                    alt={author?.username}
                                />
                                <div className='flex flex-col'>
                                    <h4 className='font-semibold text-base'>{author?.username}</h4>
                                    <span className='text-xs'>{author?.email}</span>
                                </div>
                            </div>
                            <div className='flex items-center'>
                                <div className='flex items-center mr-5'>
                                    <MdOutlineFileDownload className='text-black/40 text-lg' />
                                    <p className='text-sm font-semibold text-black/40 ml-1'>
                                        {model?.downloaded}
                                    </p>
                                </div>
                                <div className='flex items-center'>
                                    <IoEyeOutline className='text-black/40' />
                                    <p className='text-sm font-semibold text-black/40 ml-1'>
                                        {model?.viewed}
                                    </p>
                                </div>
                                <div
                                    className={classNames(
                                        'flex items-center ml-5 px-[10px]  hover:text-white hover:bg-primary hover:border-primary cursor-pointer py-1 border-[1px] border-solid  rounded-md transition-colors',
                                        {
                                            'bg-primary text-white border-primary': star,
                                            'bg-white text-black/40 border-black/30': !star,
                                        },
                                    )}
                                    onClick={starModel}
                                >
                                    <IoIosStarOutline className='' />
                                    <p className='text-sm font-semibold  ml-1'>{starNumber}</p>
                                </div>
                            </div>
                        </div>
                        {model && (
                            <DownloadLink
                                mode='long'
                                modelFolder={model?.file_name}
                                modelId={model.id}
                                downloaded={model.downloaded}
                            />
                        )}
                        <div className=' mt-4 mb-6'>
                            <p className='text-sm leading-7'>{model?.description}</p>
                        </div>
                        <div className='flex items-center mb-3'>
                            <div className='flex items-center'>
                                <p className='text-xs mr-2'>License:</p>
                                <h4 className='font-semibold text-sm'>CC Attribution</h4>
                            </div>
                            <a
                                href='https://creativecommons.org/licenses/by/4.0/'
                                rel='noreferrer'
                                target='_blank'
                                className='flex items-center text-black text-xs hover:text-blue-400 ml-2  transition-colors cursor-pointer no-underline'
                            >
                                <AiOutlineQuestionCircle className='text-base' />
                                <p className='ml-1'>Learn more</p>
                            </a>
                        </div>
                        <div className='flex items-center '>
                            <AiOutlineClockCircle className='text-base mr-2' />
                            <p className='text-xs font-bold'>Published date:</p>
                            <p className='ml-2 text-xs'>{model && formatDate(model?.created_at)}</p>
                        </div>
                    </div>
                </div>
                <div className='col-span-2'>
                    <div className='ml-7 '>
                        <h3 className='uppercase text-sm my-[10px] text-black/40 font-normal'>
                            Suggested 3D Models
                        </h3>
                        <div className='flex flex-col'>
                            {suggestModelList &&
                                suggestModelList.map((model: any) => (
                                    <ModelHorizontalCard
                                        key={model.name}
                                        model={model}
                                    ></ModelHorizontalCard>
                                ))}
                        </div>
                        {/* <div className='w-full flex justify-center items-center mt-2'>
                            <div className='uppercase text-primary text-[10px] p-1 px-2 font-semibold rounded-md border-2 border-solid border-primary hover:bg-primary/10 cursor-pointer'>
                                Load more
                            </div>
                        </div> */}
                    </div>
                </div>
            </div>
        </div>
    )
}
