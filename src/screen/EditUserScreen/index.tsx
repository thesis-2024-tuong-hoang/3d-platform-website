import { useState } from 'react'
import axios from 'axios'
import FormComponent from '~root/components/Form'
import CustomInput from '~root/components/CustomInput'
import { BASE_API_URL } from '~root/constants'
import * as Yup from 'yup'
import CustomButton from '~root/components/CustomButton'
import { useAppSelector } from '~root/config/redux/reduxHooks'
import { useNavigate } from 'react-router-dom'
import { FcApproval } from 'react-icons/fc'
import { IoCloseOutline } from 'react-icons/io5'

const ProfileSchema = Yup.object().shape({
    username: Yup.string().required('This field is required'),
    about: Yup.string(),
    city: Yup.string().required('This field is required'),
    country: Yup.string().required('This field is required'),
    profession: Yup.string().required('This field is required'),
})

interface IProfileValues {
    id?: string
    username: string
    about: string
    city: string
    country: string
    profession: string
}

const DEFAULT_PROFILE_VALUE: IProfileValues = {
    username: '',
    about: '',
    city: '',
    country: '',
    profession: '',
}

export default function EditUserScreen() {
    const [successModel, setSuccessModel] = useState<boolean>(false)
    const navigate = useNavigate()
    const userId = useAppSelector((state) => state.auth.currentUser)

    const handleSubmit = async (values: IProfileValues) => {
        const requestBody = {
            ...values,
            username: values.username,
            about: values.about,
            city: values.city,
            country: values.country,
            profession: values.profession,
            id: userId,
        }

        try {
            const response = await axios({
                method: 'PUT',
                baseURL: BASE_API_URL + `/process-data/user/`,
                data: requestBody,
            })

            if (response.status === 204) {
                setSuccessModel(true)
            }
        } catch (error) {
            console.log(error)
        }
    }

    const handleCloseSuccessModal = () => {
        setSuccessModel(false)
    }

    return (
        <div className=' w-full flex-grow relative flex justify-center items-center'>
            <img
                className='w-full h-full absolute z-0'
                src='https://wallpaperaccess.com/full/2909129.jpg'
                alt='upload-background'
            />
            <div className='w-1/2 h-fit p-6 my-20 absolute z-10  bg-white justify-center items-center gap-6 flex rounded-md '>
                {!successModel && (
                    <div className='w-full h-fit flex flex-col justify-start items-center '>
                        <div className='w-full p-8 pt-4  flex flex-col justify-start items-center '>
                            <div className='text-center text-neutral-700 text-5xl font-semibold'>
                                Edit your profile
                            </div>
                        </div>
                        <div className='w-full flex justify-between '>
                            <FormComponent
                                className='w-full'
                                initialValues={DEFAULT_PROFILE_VALUE}
                                validationSchema={ProfileSchema}
                                onSubmit={handleSubmit}
                            >
                                <CustomInput
                                    name='username'
                                    label='Username'
                                    type='text'
                                    style='TextField'
                                />
                                <CustomInput
                                    name='about'
                                    label='About'
                                    type='text'
                                    style='TextField'
                                />
                                <div className='w-full flex items-center justify-between gap-4'>
                                    <CustomInput
                                        name='city'
                                        label='City'
                                        type='text'
                                        style='TextField'
                                        className='w-full'
                                    />
                                    <CustomInput
                                        name='country'
                                        label='Country'
                                        type='text'
                                        style='TextField'
                                        className='w-full'
                                    />
                                </div>
                                <CustomInput
                                    name='profession'
                                    label='Profession'
                                    type='text'
                                    style='TextField'
                                />

                                <CustomButton
                                    className='w-full rounded-md text-xs py-4 font-semibold mt-5 bg-primary/90 hover:cursor-pointer hover:bg-primary transition-colors'
                                    title='SAVE'
                                    submit
                                />
                            </FormComponent>
                        </div>
                    </div>
                )}
                {successModel && (
                    <div className=' h-fit flex flex-col justify-center items-center'>
                        <div className='w-full flex justify-end text-4xl hover:text-primary cursor-pointer transition-colors'>
                            <IoCloseOutline onClick={handleCloseSuccessModal} />
                        </div>
                        <FcApproval className='text-9xl mb-8' />
                        <h4 className='text-4xl'>Update Profile Completed</h4>
                        <p className='mt-4 text-xl'>
                            Your profile have been updated successfully !
                        </p>
                        <CustomButton
                            className='mt-10 p-4 rounded-md bg-primary font-medium cursor-pointer'
                            title='GO TO HOME PAGE'
                            onClick={() => navigate('/', { replace: true })}
                        />
                    </div>
                )}
            </div>
        </div>
    )
}
