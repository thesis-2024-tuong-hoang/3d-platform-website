import { useCallback, useLayoutEffect, useState } from 'react'
import classNames from 'classnames'
import { useLocation, useNavigate } from 'react-router-dom'
import { MdOutlineLocationOn } from 'react-icons/md'
import ModelCard from '~root/components/ModelCard'
import axios from 'axios'
import { BASE_API_URL, DEFAULT_USER_AVATAR } from '~root/constants'
import { useAppSelector } from '~root/config/redux/reduxHooks'
import { IModelCard, formatDate } from '../DetailedModelSrceen'

export default function UserProfileScreen() {
    const { pathname } = useLocation()
    const userId = useAppSelector((state) => state.auth.currentUser)
    const userSlug = pathname?.split('/')[1]
    const navigate = useNavigate()
    const [user, setUser] = useState<any>(undefined)
    const [models, setModels] = useState<IModelCard[]>([])

    const getUserData = useCallback(async () => {
        if (userSlug) {
            const res = await axios({
                method: 'GET',
                baseURL: BASE_API_URL + `/process-data/user/${userSlug}`,
            })

            if (res.status === 200) {
                setUser(res.data.user)
            }
        }
    }, [userSlug])

    const getAllModels = useCallback(async () => {
        if (userSlug) {
            const res = await axios({
                method: 'GET',
                baseURL: BASE_API_URL + `/process-data/user/${userSlug}`,
            })

            const userId = res.data.user.id

            if (userId) {
                const modelsResponse = await axios({
                    method: 'POST',
                    baseURL: BASE_API_URL + `/process-data/3d-models`,
                    data: {
                        currentUser: userId,
                    },
                })

                const modelList = modelsResponse.data.modelList.map((model: any) => {
                    return {
                        ...model,
                        user: res.data.user,
                    }
                })

                setModels(modelList)
            }
        }
    }, [userSlug])

    useLayoutEffect(() => {
        getUserData()
        getAllModels()
    }, [getAllModels, getUserData, pathname])

    const handleNavigateToEdit = () => {
        navigate('/edit-profile', { replace: false })
    }

    return (
        <div className='flex flex-col items-center'>
            <div className='w-full max-h-40 overflow-hidden relative'>
                <img
                    src='https://img.freepik.com/premium-photo/abstract-background-with-hexagons-generative-ai_123447-15359.jpg?w=2000'
                    alt='slide'
                    className='block w-full '
                />
                <div className='w-full h-full absolute top-0 left-0 flex items-center justify-center'>
                    <div className='flex items-center max-w-7xl w-full'>
                        <img
                            src={user?.avatar || DEFAULT_USER_AVATAR}
                            alt={user?.username}
                            className='block w-24 h-24 rounded-md'
                        />
                        <div className='flex flex-col ml-5 flex-1'>
                            <div className='flex items-baseline'>
                                <h3 className='text-white font-light text-2xl py-1'>
                                    {user?.username}
                                </h3>
                                {user?.city && user?.country && (
                                    <div className='flex items-center ml-4 text-white'>
                                        <MdOutlineLocationOn className='text-xl' />
                                        <p className='text-base'>
                                            {`${user?.city}, ${user?.country}`}{' '}
                                        </p>
                                    </div>
                                )}
                            </div>
                            <p className='text-sm font-normal text-white py-1'>
                                Create Something Unique
                            </p>
                            {user?.id === userId && (
                                <div className='flex items-center py-2'>
                                    <button
                                        className='px-4 py-1 text-xs rounded-md font-semibold text-white hover:cursor-pointer bg-primary'
                                        onClick={handleNavigateToEdit}
                                    >
                                        Edit
                                    </button>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
            <div className='flex flex-col max-w-7xl w-full my-8'>
                <div className='flex items-center'>
                    <button
                        value={'summary'}
                        className={classNames(
                            'text-xs px-4 py-3   hover:cursor-pointer transition-all rounded-2xl mr-4 bg-primary text-white',
                        )}
                    >
                        SUMMARY
                    </button>
                </div>
                <div className='flex mt-8'>
                    <div className='w-2/3 pr-8'>
                        <div className='flex justify-between items-center'>
                            <h4 className='text-black/50 text-sm font-medium mb-3'>
                                POPULAR 3D MODELS
                            </h4>
                        </div>
                        <div className='w-full grid grid-cols-2 gap-8'>
                            {models.map((model) => (
                                <ModelCard key={model.id} model={model} disableAuthor />
                            ))}
                            {models.length === 0 && (
                                <div className='w-full col-span-2 flex items-center h-80 text-xl justify-center text-black/50'>
                                    <p>There is no any uploaded model</p>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className='w-1/3 '>
                        <div className='px-5 pt-5 pb-1 flex flex-col bg-white rounded-md'>
                            <div className='flex flex-col mb-2'>
                                <h3 className='text-xs text-black/40 font-medium'>ABOUT</h3>
                                <p className='my-2 text-base'>
                                    {user?.about ? user?.about : '---'}
                                </p>
                            </div>
                            <div className='flex flex-col mb-2'>
                                <h3 className='text-xs text-black/40 font-medium'>CATEGORY</h3>
                                <p className='my-2 text-base'>
                                    {user?.profession ? user?.profession : '---'}
                                </p>
                            </div>
                            <div className='flex flex-col mb-2'>
                                <h3 className='text-xs text-black/40 font-medium'>MEMBER SINCE</h3>
                                <p className='my-2 text-base'>{formatDate(user?.created_at)}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
