import { nameRegex, emailRegex } from './regex'

export const validateGenericName = (name: string) => {
    const regex = /^[a-z ,.'-]+$/i
    if (name) {
        if (name.match(regex)) {
            return null
        } else {
            return 'Invalid name'
        }
    } else {
        return 'This field is required'
    }
}

export const validateEmail = (email: string) => {
    const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

    if (email) {
        if (email.match(validRegex)) {
            return null
        } else {
            return 'Invalid email address'
        }
    } else {
        return 'Please enter a valid email address'
    }
}

export const validatePassword = (password: string) => {
    if (password) {
        if (password.length >= 6) {
            return null
        } else {
            return 'Password must be at least 6 characters'
        }
    } else {
        return 'This field is required'
    }
}

export const validateForm = (formObject: any) => {
    const keys = Object.keys(formObject)
    const formIsValid: any[] = []
    keys.forEach((key) => {
        if (key === 'firstname' || key === 'lastname') {
            if (formObject[key].match(nameRegex) && formObject[key].length > 0) {
                formIsValid.push(true)
            } else {
                formIsValid.push(false)
            }
        }
        if (key === 'email') {
            if (formObject[key].match(emailRegex)) {
                formIsValid.push(true)
            } else {
                formIsValid.push(false)
            }
        }
        if (key === 'password') {
            if (formObject[key].length >= 6) {
                formIsValid.push(true)
            } else {
                formIsValid.push(false)
            }
        }
        if (key === 'confirm-password') {
            if (formObject[key] === formObject.password) {
                formIsValid.push(true)
            } else {
                formIsValid.push(false)
            }
        }
    })
    return formIsValid.every((value) => value === true)
}
